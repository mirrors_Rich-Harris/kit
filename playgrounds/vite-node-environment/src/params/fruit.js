/**
 * @param {string} param
 */
export function match(param) {
	return param === 'apple' || param === 'orange';
}
